export default class customValidation {
  constructor(input) {
    this.invalidities = [];
    this.validityChecks = [];

    this.inputNode = input;
    this.registerListener();
  }
  addInvalidity(message) {
    this.invalidities.push(message);
  }
  getInvalidities() {
    return this.invalidities.join('.\n');
  }
  checkValidity(input) {
    for (let i = 0; i < this.validityChecks.length; i++) {
      let isInvalid = this.validityChecks[i].isInvalid(input);
      if (isInvalid) {
        this.addInvalidity(this.validityChecks[i].invalidityMessage);
      }

      let requirementElement = this.validityChecks[i].element;
      if (requirementElement) {
        if (isInvalid) {
          requirementElement.classList.add('invalid');
          requirementElement.classList.remove('valid');
        } else {
          requirementElement.classList.remove('invalid');
          requirementElement.classList.add('valid');
        }
      }
    }
  }
  checkInput() {
    this.inputNode.customValidation.invalidities = [];
    this.checkValidity(this.inputNode);
    if (
      this.inputNode.customValidation.invalidities.length === 0 &&
      this.inputNode.value !== ''
    ) {
      this.inputNode.setCustomValidity('');
    } else {
      let message = this.inputNode.customValidation.getInvalidities();
      this.inputNode.setCustomValidity(message);
    }
  }
  registerListener() {
    let customValidation = this;

    this.inputNode.addEventListener('keyup', function () {
      customValidation.checkInput();
    });
  }
}
