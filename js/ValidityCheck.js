import { confirmedPasswordInput, passwordInput } from './index.js';

/*-----------------------------------------------------------------

Arrays with the validity checks for each input

-----------------------------------------------------------------*/

const validUrlRegex =
  /(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]+\.[^\s]{2,}|www\.[a-zA-Z0-9]+\.[^\s]{2,})/g;
const validEmailRegex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
const validPhoneRequex = /^\(?([0-9]{4})\)?[-]([0-9]{4})$/;
export let firstNameValidityCheck = [
  {
    isInvalid: function (input) {
      return input.value.length < 1;
    },
    invalidityMessage: 'This field cannot be empty',
    element: document.querySelector(
      'label[for="firstName"] .input-requirements li:nth-child(1)'
    ),
  },
];
export let lastNameValidityCheck = [
  {
    isInvalid: function (input) {
      return input.value.length < 1;
    },
    invalidityMessage: 'This field cannot be empty',
    element: document.querySelector(
      'label[for="lastName"] .input-requirements li:nth-child(1)'
    ),
  },
];
export let emailValidityChecks = [
  {
    isInvalid: function (input) {
      return !input.value.match(validEmailRegex);
    },
    invalidityMessage: 'Input must be a valid Email',
    element: document.querySelector(
      'label[for="email"] .input-requirements li:nth-child(1)'
    ),
  },
];
export let passwordValidityCheck = [
  {
    isInvalid: function (input) {
      return input.value.length < 8;
    },
    invalidityMessage: 'This input needs to be at least 8 characters long',
    element: document.querySelector(
      'label[for="password"] .input-requirements li:nth-child(1)'
    ),
  },
  {
    isInvalid: function (input) {
      return !input.value.match(/[0-9]/g);
    },
    invalidityMessage: 'At least 1 number is required',
    element: document.querySelector(
      'label[for="password"] .input-requirements li:nth-child(2)'
    ),
  },
  {
    isInvalid: function (input) {
      return !input.value.match(/[a-z]/g);
    },
    invalidityMessage: 'At least 1 lowercase letter is required',
    element: document.querySelector(
      'label[for="password"] .input-requirements li:nth-child(3)'
    ),
  },
  {
    isInvalid: function (input) {
      return !input.value.match(/[A-Z]/g);
    },
    invalidityMessage: 'At least 1 uppercase letter is required',
    element: document.querySelector(
      'label[for="password"] .input-requirements li:nth-child(4)'
    ),
  },
  {
    isInvalid: function (input) {
      return !input.value.match(/[\!\@\#\$\%\^\&\*]/g);
    },
    invalidityMessage: 'You need one of the required special characters',
    element: document.querySelector(
      'label[for="password"] .input-requirements li:nth-child(5)'
    ),
  },
];
export let confirmedPasswordValidityChecks = [
  {
    isInvalid: function () {
      return confirmedPasswordInput.value != passwordInput.value;
    },
    invalidityMessage: 'This password needs to match the first one',
    element: document.querySelector(
      'label[for="confirmPassword"] .input-requirements li:nth-child(1)'
    ),
  },

  {
    isInvalid: function (input) {
      return input.value.length < 1;
    },
    invalidityMessage: 'This field cannot be empty',
    element: document.querySelector(
      'label[for="confirmPassword"] .input-requirements li:nth-child(2)'
    ),
  },
];
export let phoneNumberValidityCheck = [
  {
    isInvalid: function (input) {
      return !input.value.match(validPhoneRequex);
    },
    invalidityMessage:
      'This phone number does not meet the requierd format (XXXX-XXXX)',
    element: document.querySelector(
      'label[for="phoneNumber"] .input-requirements li:nth-child(1)'
    ),
  },
];
export let ageInputValidityCheck = [
  {
    isInvalid: function (input) {
      return !input.value.match(/^([1-9]|[1-9][0-9]|1[01][0-9]|12[0-7])$/);
    },
    invalidityMessage: 'This field accepts only numbers between 1 and 100',
    element: document.querySelector(
      'label[for="age"] .input-requirements li:nth-child(1)'
    ),
  },
];
export let websiteInputValidityCheck = [
  {
    isInvalid: function (input) {
      return !input.value.match(validUrlRegex);
    },
    invalidityMessage: 'This is not a valid URL',
    element: document.querySelector(
      'label[for="website"] .input-requirements li:nth-child(1)'
    ),
  },
];
export let experienceInputValidityCheck = [
  {
    isInvalid: function (input) {
      return !(
        input.value == '0' ||
        input.value == '50' ||
        input.value == '100'
      );
    },
    invalidityMessage: 'pick one of the available options',
  },
];
export let agrementInputValidityCheck = [
  {
    isInvalid: function (input) {
      return input.checked !== true;
    },
    invalidityMessage: 'Must agree with the terms to continue',
  },
];
