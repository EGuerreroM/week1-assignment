import customValidation from './CustomValidation.js';
import {
  firstNameValidityCheck,
  lastNameValidityCheck,
  emailValidityChecks,
  passwordValidityCheck,
  confirmedPasswordValidityChecks,
  phoneNumberValidityCheck,
  ageInputValidityCheck,
  websiteInputValidityCheck,
  experienceInputValidityCheck,
  agrementInputValidityCheck,
} from './ValidityCheck.js';
/*-----------------------------------------------------------------

Setup prototypes for every input

-----------------------------------------------------------------*/
let firstNameInput = document.getElementById('firstName');
let lastNameInput = document.getElementById('lastName');
let emailInput = document.getElementById('email');
export let passwordInput = document.getElementById('password');
export let confirmedPasswordInput = document.getElementById('confirmPassword');
let phoneNumberInput = document.getElementById('phoneNumber');
let ageInput = document.getElementById('age');
let websiteInput = document.getElementById('website');
let experienceInput = document.getElementById('experience');
let agrementInput = document.getElementById('aprovement');

firstNameInput.customValidation = new customValidation(firstNameInput);
firstNameInput.customValidation.validityChecks = firstNameValidityCheck;

lastNameInput.customValidation = new customValidation(lastNameInput);
lastNameInput.customValidation.validityChecks = lastNameValidityCheck;

emailInput.customValidation = new customValidation(emailInput);
emailInput.customValidation.validityChecks = emailValidityChecks;

passwordInput.customValidation = new customValidation(passwordInput);
passwordInput.customValidation.validityChecks = passwordValidityCheck;

confirmedPasswordInput.customValidation = new customValidation(
  confirmedPasswordInput
);
confirmedPasswordInput.customValidation.validityChecks =
  confirmedPasswordValidityChecks;

phoneNumberInput.customValidation = new customValidation(phoneNumberInput);
phoneNumberInput.customValidation.validityChecks = phoneNumberValidityCheck;

ageInput.customValidation = new customValidation(ageInput);
ageInput.customValidation.validityChecks = ageInputValidityCheck;

websiteInput.customValidation = new customValidation(websiteInput);
websiteInput.customValidation.validityChecks = websiteInputValidityCheck;

experienceInput.customValidation = new customValidation(experienceInput);
experienceInput.customValidation.validityChecks = experienceInputValidityCheck;

agrementInput.customValidation = new customValidation(agrementInput);
agrementInput.customValidation.validityChecks = agrementInputValidityCheck;

/*-----------------------------------------------------------------

Event Listeners

-----------------------------------------------------------------*/

let inputs = document.querySelectorAll('input:not([type="submit"])');
let submit = document.querySelector('input[type="submit"');
let form = document.getElementById('registerForm');

let modal = document.getElementById('gif-modal');

window.addEventListener('click', (event) => {
  if (event.target == modal) {
    modal.style.display = 'none';
  }
});

let keyPressed = [];
const secretCode = [
  'ArrowUp',
  'ArrowUp',
  'ArrowDown',
  'ArrowDown',
  'ArrowRight',
  'ArrowLeft',
  'KeyA',
  'KeyB',
];

window.addEventListener('keyup', (event) => {
  keyPressed.push(event.code);
  keyPressed.splice(
    -secretCode.length - 1,
    keyPressed.length - secretCode.length
  );

  if (keyPressed.join() == secretCode.join()) {
    modal.style.display = 'flex';
  }
});

function validate() {
  inputs.forEach((input) => {
    input.customValidation.checkInput();
  });
}

submit.addEventListener('click', validate);
form.addEventListener('submit', (e) => {
  e.preventDefault();

  inputs.forEach((input) => {
    if (input.id == 'experience' || input.id == 'aprovment') {
      switch (input.value) {
        case '0':
          console.log(input.id + ':Noob');
          break;
        case '50':
          console.log(input.id + ':Casual');
          break;
        case '100':
          console.log(input.id + ':Expert');
          break;
        default:
          break;
      }
    } else {
      console.log(input.id + ': ' + input.value);
    }
  });
});
