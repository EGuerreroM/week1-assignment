# week1-assignment

Form validation with Vanilla Javascript

The form has a secret code when you press the key sequence 'ArrowUp', 'ArrowUp', 'ArrowDown', 'ArrowDown','ArrowRight','ArrowLeft','KeyA','KeyB'

You can see a preview of the form in the next link:
https://week1-assignment.vercel.app/
